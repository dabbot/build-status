# build-status

A simple checkup to view the build status of repositories with CI.

Cache: [![cache-ci-badge][]][cache-ci]

Radios: [![radios-ci-badge][]][radios-ci]

Worker: [![worker-ci-badge][]][worker-ci]

[cache-ci]: https://travis-ci.org/dabbotorg/cache
[cache-ci-badge]: https://img.shields.io/travis/dabbotorg/cache.svg?style=flat-square
[radios-ci]: https://travis-ci.org/dabbotorg/radios
[radios-ci-badge]: https://img.shields.io/travis/dabbotorg/radios.svg?style=flat-square
[worker-ci]: https://travis-ci.org/dabbotorg/worker
[worker-ci-badge]: https://img.shields.io/travis/dabbotorg/worker.svg?style=flat-square
